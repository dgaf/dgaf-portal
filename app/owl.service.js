/**
 * Created by melisoner on 6/13/16.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import the Injectablt function
var core_1 = require('@angular/core');
var mock_owls_1 = require('./mock-owls');
//apply the injectable function with the decorator: @Injectable()
//It is a "best practice" to add the decorator function from start even though you don't have any dependencies.
var OwlService = (function () {
    function OwlService() {
    }
    OwlService.prototype.getOwls = function () {
        return Promise.resolve(mock_owls_1.OWLS);
        ;
    };
    OwlService.prototype.getOwlsSlowly = function () {
        return new Promise(function (resolve) {
            return setTimeout(function () { return resolve(mock_owls_1.OWLS); }, 2000);
        });
    };
    OwlService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], OwlService);
    return OwlService;
})();
exports.OwlService = OwlService;
//# sourceMappingURL=owl.service.js.map