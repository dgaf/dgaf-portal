/**
 * Created by melisoner on 6/13/16.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import statement references the things we need.
//import angular/core library(module) so @component decorator can access it.
var core_1 = require('@angular/core');
var owl_detail_component_1 = require('./owl-detail.component');
var owl_service_1 = require('./owl.service');
//@component decorator tells Angular what template to use and how to create the component
//Component is a decorator function that takes a metadata object as an argument.
//Apply this function to the component class by prefixing the function with the `@` symbol
var AppComponent = (function () {
    function AppComponent(owlService) {
        this.owlService = owlService;
        this.title = 'Tome Owls Weekly Reports';
    }
    AppComponent.prototype.getOwls = function () {
        var _this = this;
        this.owlService.getOwls().then(function (owls) { return _this.owls = owls; });
    };
    AppComponent.prototype.ngOnInit = function () {
        this.getOwls();
    };
    AppComponent.prototype.onSelect = function (owl) { this.selectedOwl = owl; };
    ;
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n        <h1>{{title}}</h1>\n        <h2> Tome Owls </h2>\n        <ul class=\"owls\">\n        <!--The leading asterisk (*) in front of ngFor is a critical part of this syntax.-->\n            <li *ngFor=\"let owl of owls\" [class.selected]=\"owl === selectedOwl\" (click)=\"onSelect(owl)\" >\n                <span class=\"badge\">{{owl.id}}</span> {{owl.name}}\n            </li>\n        </ul>\n        <my-owl-detail [owl]=\"selectedOwl\"></my-owl-detail>\n    ",
            styles: ["\n      .selected {\n        background-color: #CFD8DC !important;\n        color: white;\n      }\n      .owls {\n        margin: 0 0 2em 0;\n        list-style-type: none;\n        padding: 0;\n        width: 15em;\n      }\n      .owls li {\n        cursor: pointer;\n        position: relative;\n        left: 0;\n        background-color: #EEE;\n        margin: .5em;\n        padding: .3em 0;\n        height: 1.6em;\n        border-radius: 4px;\n      }\n      .owls li.selected:hover {\n        background-color: #BBD8DC !important;\n        color: white;\n      }\n      .owls li:hover {\n        color: #607D8B;\n        background-color: #DDD;\n        left: .1em;\n      }\n      .owls .text {\n        position: relative;\n        top: -3px;\n      }\n      .owls .badge {\n        display: inline-block;\n        font-size: small;\n        color: white;\n        padding: 0.8em 0.7em 0 0.7em;\n        background-color: #607D8B;\n        line-height: 1em;\n        position: relative;\n        left: -1px;\n        top: -4px;\n        height: 1.8em;\n        margin-right: .8em;\n        border-radius: 4px 0 0 4px;\n      }\n"],
            directives: [owl_detail_component_1.OwlDetailComponent],
            providers: [owl_service_1.OwlService]
        }), 
        __metadata('design:paramtypes', [owl_service_1.OwlService])
    ], AppComponent);
    return AppComponent;
})();
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map