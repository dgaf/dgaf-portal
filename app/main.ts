/**
 * Created by melisoner on 6/13/16.
 */

//1- import angular's browser bootstrap function
import { bootstrap }    from '@angular/platform-browser-dynamic';

//2- import the application root component,AppComponent
import { AppComponent } from './app.component';


//call bootstrap with the AppComponent
bootstrap(AppComponent);