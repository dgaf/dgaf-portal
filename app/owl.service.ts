/**
 * Created by melisoner on 6/13/16.
 */

//import the Injectablt function
import { Injectable } from '@angular/core';
import { Owl } from './owl';
import { OWLS } from './mock-owls';

//apply the injectable function with the decorator: @Injectable()
//It is a "best practice" to add the decorator function from start even though you don't have any dependencies.
@Injectable()
export class OwlService {

    getOwls(){
        return Promise.resolve(OWLS);;
    }

    getOwlsSlowly(){
        return new Promise<Owl[]>(resolve =>
            setTimeout(()=>resolve(OWLS), 2000)
        );
    }
}