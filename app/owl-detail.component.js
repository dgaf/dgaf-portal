/**
 * Created by melisoner on 6/13/16.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var owl_1 = require('./owl');
var OwlDetailComponent = (function () {
    function OwlDetailComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', owl_1.Owl)
    ], OwlDetailComponent.prototype, "owl", void 0);
    OwlDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-owl-detail',
            template: "\n    <div *ngIf=\"owl\">\n        <h2>{{owl.name}}'s Weekly Report</h2>\n        <div><label>id: </label>{{owl.id}}</div>\n        <div>\n            <label>name: </label>\n            <!--<input value=\"{{owl.name}}\" placeholder=\"name\">-->\n            <input [(ngModel)]=\"owl.name\" placeholder=\"name\" />\n        </div>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], OwlDetailComponent);
    return OwlDetailComponent;
})();
exports.OwlDetailComponent = OwlDetailComponent;
//# sourceMappingURL=owl-detail.component.js.map