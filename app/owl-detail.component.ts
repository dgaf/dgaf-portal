/**
 * Created by melisoner on 6/13/16.
 */


import { Component, Input } from '@angular/core';

import { Owl } from './owl';

@Component({
    selector: 'my-owl-detail',

    template: `
    <div *ngIf="owl">
        <h2>{{owl.name}}'s Weekly Report</h2>
        <div><label>id: </label>{{owl.id}}</div>
        <div>
            <label>name: </label>
            <!--<input value="{{owl.name}}" placeholder="name">-->
            <input [(ngModel)]="owl.name" placeholder="name" />
        </div>
    </div>
    `
})

export class OwlDetailComponent {
    @Input()
    owl: Owl;
}