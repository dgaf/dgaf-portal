/**
 * Created by melisoner on 6/13/16.
 */


//import statement references the things we need.
//import angular/core library(module) so @component decorator can access it.
import { Component, OnInit } from '@angular/core';

import { Owl } from './owl';
import { OwlDetailComponent } from './owl-detail.component';
import { OwlService } from './owl.service';


//@component decorator tells Angular what template to use and how to create the component
//Component is a decorator function that takes a metadata object as an argument.
//Apply this function to the component class by prefixing the function with the `@` symbol

@Component({
    selector: 'my-app',
    template: `
        <h1>{{title}}</h1>
        <h2> Tome Owls </h2>
        <ul class="owls">
        <!--The leading asterisk (*) in front of ngFor is a critical part of this syntax.-->
            <li *ngFor="let owl of owls" [class.selected]="owl === selectedOwl" (click)="onSelect(owl)" >
                <span class="badge">{{owl.id}}</span> {{owl.name}}
            </li>
        </ul>
        <my-owl-detail [owl]="selectedOwl"></my-owl-detail>
    `,
    styles: [`
      .selected {
        background-color: #CFD8DC !important;
        color: white;
      }
      .owls {
        margin: 0 0 2em 0;
        list-style-type: none;
        padding: 0;
        width: 15em;
      }
      .owls li {
        cursor: pointer;
        position: relative;
        left: 0;
        background-color: #EEE;
        margin: .5em;
        padding: .3em 0;
        height: 1.6em;
        border-radius: 4px;
      }
      .owls li.selected:hover {
        background-color: #BBD8DC !important;
        color: white;
      }
      .owls li:hover {
        color: #607D8B;
        background-color: #DDD;
        left: .1em;
      }
      .owls .text {
        position: relative;
        top: -3px;
      }
      .owls .badge {
        display: inline-block;
        font-size: small;
        color: white;
        padding: 0.8em 0.7em 0 0.7em;
        background-color: #607D8B;
        line-height: 1em;
        position: relative;
        left: -1px;
        top: -4px;
        height: 1.8em;
        margin-right: .8em;
        border-radius: 4px 0 0 4px;
      }
`],
    directives: [OwlDetailComponent],
    providers:[OwlService]

})
//component class controls the appearance and behavior of a view through its template
//this is initially an empty class and it can be expanded with properties and application logic
export class AppComponent implements OnInit {
    title = 'Tome Owls Weekly Reports';
    owls: Owl[];
    selectedOwl: Owl;

    constructor(private owlService: OwlService) {  }

    getOwls() {
        this.owlService.getOwls().then(owls => this.owls = owls);
    }

    ngOnInit(){
        this.getOwls();
    }

    onSelect(owl: Owl){ this.selectedOwl = owl; };
}

