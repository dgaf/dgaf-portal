/**
 * Created by melisoner on 6/13/16.
 */

    import { Owl } from "./owl";

export var OWLS:Owl[] = [
    {"id":1, name:"Melis"},
    {"id": 11, "name": "Phil"},
    {"id": 12, "name": "Andy"},
    {"id": 13, "name": "Matt"},
    {"id": 14, "name": "Angela"},
    {"id": 15, "name": "Nick"},
    {"id": 16, "name": "Barch"},
    {"id": 17, "name": "Steve"},
    {"id": 18, "name": "Ben"},
    {"id": 19, "name": "John"}
]
