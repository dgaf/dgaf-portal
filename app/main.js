/**
 * Created by melisoner on 6/13/16.
 */
//1- import angular's browser bootstrap function
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
//2- import the application root component,AppComponent
var app_component_1 = require('./app.component');
//call bootstrap with the AppComponent
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent);
//# sourceMappingURL=main.js.map